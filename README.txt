This is a Drush extension that can be used to quickly see which users last
accessed and logged into your site. I wrote it to give me a bit more 
information for planning the best time to carry out scheduled maintenance
on on a multisite configuration that has users spanning several timezones.

INSTALLATION

As with all Drush extensions, put the directory somewhere that Drush looks
for additional commands, for example in ~/.drush/ Currently tested using
Drush-5.x and Drush-6.x on Drupal-6.x, Drupal-7.x, and Drupal-8.x-dev

EXAMPLE USAGE

drush lastlogin
  Show all users with how long ago they accessed and logged into the site.

drush lastlogin --users=3
  Show the latest three users.

drush lastlogin --exclude-uid1
  Show all users, except uid1.

drush lastlogin --mins=30
  Show all users within the last 30 minutes.

drush lastlogin --hours=2 --mins=30
  Show all users within the last two and a half hours.

drush lastlogin --weeks=6
  Show all users within the last six weeks.

drush lastlogin --months=1 --weeks=2
  Show all within the last six weeks, approximately.

drush lastlogin --years=1
  Show all users within the past year.

drush lastlogin --users=5 --exclude-uid1 --days=1 --hours=12
  Show users from the last 36 hours, limited to the most recent five,
  and excluding superuser.

drush lastlogin --help
  See more detailed documentation.

This is a sandbox project and the first Drush extension I've written so
feedback/suggestions/comments/help much appreciated!


