<?php

/**
 * @file
 * Drush command to check access and login times for users.
 */

/**
 * Implements hook_drush_command().
 */
function lastlogin_drush_command() {
  $items = array();

  $items['lastlogin'] = array(
    'description' => 'Print a table showing how long ago users last accessed and logged into a site.',
    'options' => array(
      'exclude-uid1' => array(
        'description' => 'Option to exclude uid1 from returned results.',
      ),
      'users' => array(
        'description' => 'Option to limit number of users returned.',
        'example-value' => '5',
      ),
      'mins' => array(
        'description' => 'Time-based option to limit to a certain number of minutes ago.',
        'example-value' => '30',
      ),
      'hours' => array(
        'description' => 'Time-based option to limit to a certain number of hours ago.',
        'example-value' => '12',
      ),
      'days' => array(
        'description' => 'Time-based option to limit to a certain number of days ago.',
        'example-value' => '3',
      ),
      'weeks' => array(
        'description' => 'Time-based option to limit to a certain number of weeks ago.',
        'example-value' => '2',
      ),
      'months' => array(
        'description' => 'Time-based option to limit to a certain number of months ago.',
        'example-value' => '6',
      ),
      'years' => array(
        'description' => 'Time-based option to limit to a certain number of years ago.',
        'example-value' => '1',
      ),
    ),
    'examples' => array(
      'drush lastlogin' => 'Show all users with how long ago they accessed and logged into the site.',
      'drush lastlogin --users=3' => 'Show the latest three users.',
      'drush lastlogin --exclude-uid1' => 'Show all users, except uid1.',
      'drush lastlogin --mins=30' => 'Show all users within the last 30 minutes.',
      'drush lastlogin --hours=2 --mins=30' => 'Show all users within the last two and a half hours.',
      'drush lastlogin --weeks=6' => 'Show all users within the last six weeks.',
      'drush lastlogin --months=1 --weeks=2' => 'Show all within the last six weeks, approximately.',
      'drush lastlogin --years=1' => 'Show all users within the past year.',
      'drush lastlogin --users=5 --exclude-uid1 --days=1 --hours=12' => 'Show users from the last 36 hours, limited to the most recent five, and excluding superuser.',
    ),
    'aliases' => array('ll'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );
  return $items;
}

/**
 * Implements hook_drush_command_validate().
 */
function drush_lastlogin_validate() {
  // Validate users option.
  $users = drush_get_option('users');
  if ((!is_numeric($users)) && (!is_null($users))) {
    return drush_set_error('DRUSH_LASTLOGIN_FAIL', dt('"@users" is not a valid argument for --users. Please specify a positive integer.', array('@users' => $users)));
  }

  // Validate the time-based options.
  $timeperiods = array('mins', 'hours', 'days', 'weeks', 'months', 'years');
  foreach ($timeperiods as $timeperiod) {
    // Fetch each time-related option for validation.
    $timeoption = (drush_get_option($timeperiod));
    if (!((is_null($timeoption) || (is_bool($timeoption))))) {
      if (!is_numeric($timeoption)) {
        return drush_set_error('DRUSH_LASTLOGIN_FAIL', dt('"@period" is not a valid argument for --' . $timeperiod . '. Please specify a positive integer.', array('@period' => $timeoption)));
      }
    }
  }
}

/**
 * Main drush command callback.
 */
function drush_lastlogin() {
  $users = drush_get_option('users', NULL);

  if (drush_get_option('exclude-uid1', FALSE)) {
    // If yes, then set query to return rows after uid 1
    // i.e. exclude admin in returned results.
    $uidopt = '1';
  }
  else {
    // If no, then set query to return rows after uid 0
    // i.e. include admin in returned results.
    $uidopt = '0';
  }

  $totalunixtime = 0;
  // Add up the total value of the time-based options, converted to seconds.
  $totalunixtime = $totalunixtime + ((drush_get_option('mins', NULL)) * 60);
  $totalunixtime = $totalunixtime + ((drush_get_option('hours', NULL)) * 60 * 60);
  $totalunixtime = $totalunixtime + ((drush_get_option('days', NULL)) * 60 * 60 * 24);
  $totalunixtime = $totalunixtime + ((drush_get_option('weeks', NULL)) * 60 * 60 * 24 * 7);
  $totalunixtime = $totalunixtime + ((drush_get_option('months', NULL)) * 60 * 60 * 24 * 30);
  $totalunixtime = $totalunixtime + ((drush_get_option('years', NULL)) * 60 * 60 * 24 * 365);

  // Get current timestamp.
  $now = time();
  // Subtract the total of all the time-based option values.
  $timelimit = ($now - $totalunixtime);
  // But if no time-based values were used just "reset" time limit
  // so all records are returned from DB regardless of timestamp.
  if ($timelimit == $now) {
    $timelimit = 0;
  }
  // Now build database query.
  $rsc = drush_db_select('users',
                          array('name', 'access', 'login'),
                          'uid > :uidno AND access >= :timelimit',
                          array(':uidno' => $uidopt, ':timelimit' => $timelimit),
                          NULL,
                          $users,
                          'access',
                          'DESC');
  if ($rsc === FALSE) {
    return drush_log(dt('Problem with querying database. Aborting.'));
  }

  // Now start building output.
  $all_users = array();
  // Add the header row.
  $all_users[] = array(dt('USER'), dt('LAST ACCESS'), dt('LAST LOGIN'));

  while ($result = drush_db_fetch_object($rsc)) {
    // Make the returned timestamps human-readable
    // and formatted as time ago.
    if ($result->access == '0') {
      $cleanaccess = 'Never';
    }
    else {
      $cleanaccess = (format_interval($now - ($result->access), 2) . dt(' ago'));
    }
    if ($result->login == '0') {
      $cleanlogin = 'Never';
    }
    else {
      $cleanlogin = (format_interval($now - ($result->login), 2) . dt(' ago'));
    }
    $all_users[] = array(($result->name), $cleanaccess, $cleanlogin);
  }
  drush_print_table($all_users, TRUE, array('25', '25', '25'));
}
